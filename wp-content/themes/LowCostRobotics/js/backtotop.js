// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("topBtn").style.display = "block";
    } else {
        document.getElementById("topBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}

//navi on Top
jQuery("document").ready(function($){
    var nav = $('#menu');

    jQuery(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
            nav.addClass("sticky");
        } else {
            nav.removeClass("sticky");
        }
    });
});

//Open Newsletter Button
function showDiv() {
   document.getElementById('newsletterform').style.display = "block";
}

