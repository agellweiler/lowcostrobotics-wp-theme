<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 08.11.2017
 */
?>
<footer>
	<div class="container">
        <div class="row">
			<nav class="col-sm-6 col-sm-offset-3">
				<?php
				$args = array(
					'theme_location' => 'nav_footer',
					'depth' => 1
				);
				wp_nav_menu( $args );
				?>
			</nav>
			<div class="col-sm-3">
				<p>sponsored by igus<sup>®</sup></p>
			</div>
        </div>
		<?php
		$permalink = get_permalink();
		$find_h = '#^http(s)?://#';
		$replace = '';
		$output = preg_replace( $find_h, $replace, $permalink )
		?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shariff/1.24.1/shariff.complete.css" integrity="sha256-XWbdCXXqb1Rd5f35G/t3hdsuuZJQ5RvT3LCekNUDa4k=" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.7/jquery.bxslider.min.css" integrity="sha256-BWIL2wbr0r8d2MmLoTbscWJ6kWRtsaX+s0uG7C6wyO4=" crossorigin="anonymous">	
        <div data-lang="de" data-theme="standard" data-services="[&quot;facebook&quot;,&quot;twitter&quot;,&quot;googleplus&quot;,&quot;linkedin&quot;,&quot;xing&quot;]" class="shariff">
			<ul class="theme-standard orientation-horizontal col-5">
				<li class="shariff-button facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_site_url(); ?>%2F" data-rel="popup" title="Bei Facebook teilen" role="button" aria-label="Bei Facebook teilen"><span class="fa fa-facebook"></span><span class="share_text">teilen</span></a></li>
				<li class="shariff-button twitter"><a href="https://twitter.com/intent/tweet?text=Low%20Cost%20Robotics%20%7C%20innovativ%20kosteng%C3%BCnstig%20flexibel&amp;url=<?php echo get_site_url(); ?>%2F" data-rel="popup" title="Bei Twitter teilen" role="button" aria-label="Bei Twitter teilen"><span class="fa fa-twitter"></span><span class="share_text">tweet</span></a></li>
				<li class="shariff-button googleplus"><a href="https://plus.google.com/share?url=<?php echo get_site_url(); ?>%2F" data-rel="popup" title="Bei Google+ teilen" role="button" aria-label="Bei Google+ teilen"><span class="fa fa-google-plus"></span><span class="share_text">teilen</span></a></li>
				<li class="shariff-button linkedin"><a href="https://www.linkedin.com/cws/share?url=<?php echo get_site_url(); ?>%2F" data-rel="popup" title="Bei LinkedIn teilen" role="button" aria-label="Bei LinkedIn teilen"><span class="fa fa-linkedin"></span><span class="share_text">mitteilen</span></a></li>
			</ul>
        </div>
	</div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="<?php echo get_bloginfo( 'template_url' ); ?>/js/backtotop.js"></script>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/slick.min.js"></script>

<script>
jQuery(document).ready(function(){
  jQuery('.slickslider').slick({
	 dots: true,
	 infinite: true,
	 speed: 300,
	 slidesToShow: 1,
	 adaptiveHeight: true,
	 arrows: true
  });
});
</script>
</body>
</html>

