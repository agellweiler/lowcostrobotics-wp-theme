<?php/*Template Name: Get E-Book*/?>
<?php get_header(); ?>
<body>
	<nav id="menu">
		<div class="backlink">
			<div class="container"><a href="<?php echo get_site_url(); ?>">Back to the start page</a></div>
		</div>
	</nav>	
	 <div id="top" class="shinyblue substage">
		<div class="container">
			<h1 class="heading style-h2"><?php the_title(); ?></h1>
		</div>
    </div>
	<a id="back-to-top" href="#top"></a>
    <div id="flexibel" class="container anchor">
		<section>
			<article>
				<div class="text">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
				the_content();
				endwhile;
				else :
				 get_template_part( 'template_parts/content', 'error' );  endif; ?>
				</div>
			</article>
		</section>
	</div>
	
	<?php
	$args = array(
		post_type => 'calltoaction',
	);
	$loop1 = new WP_Query( $args );
	if ( $loop1->have_posts() ) : while ( $loop1->have_posts() ) : $loop1->the_post();
	if( has_term( 'call_to_action6', 'Startpage' ) ) { 
			get_template_part( 'template_parts/content', 'shinyblue' ); 
	}
			endwhile;
	else : get_template_part( 'template_parts/content', 'error' );
	endif; ?>
<?php	
	$args = array(
	post_type => 'innovations',	
);
$loop14 = new WP_Query( $args );
if ( $loop14->have_posts() ) : while ( $loop14->have_posts() ) : $loop14->the_post();
if( has_term( 'Newsletter', 'Startpage' ) ) { 
get_template_part( 'template_parts/content', 'newsletter' ); 
}
endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;
	
?>

<?php get_footer(); ?>