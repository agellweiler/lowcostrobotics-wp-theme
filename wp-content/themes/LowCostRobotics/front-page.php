<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 08.11.2017
 */
?>
<?php get_header(); ?>
<?php get_template_part( 'template_parts/navi' ); ?>
<button onclick="topFunction()" id="topBtn" title="Go to top">Back to top</button>
<?php
//Loop through topteaser
$args = array(
	post_type => 'innovations',
);
$loop1 = new WP_Query( $args );
if ( $loop1->have_posts() ) : while ( $loop1->have_posts() ) : $loop1->the_post();
		if( has_term( 'Topteaser', 'Startpage' ) ) { 
		 get_template_part( 'template_parts/content', 'intro' ); 	
		}
	 endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif; 
//Call to Action1
$args = array(
	post_type => 'calltoaction',
);
$loop2 = new WP_Query( $args );
if ( $loop2->have_posts() ) : while ( $loop2->have_posts() ) : $loop2->the_post();
		if( has_term( 'call_to_action', 'Startpage' ) ) { 
		get_template_part( 'template_parts/content', 'shinyblue' );
		}
	 endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;?>

<div id="Innovative"></div>

<?php
//section-intro
$args = array(
	post_type => 'innovations',
);
$loop3 = new WP_Query( $args );
if ( $loop3->have_posts() ) : while ( $loop3->have_posts() ) : $loop3->the_post();
if( has_term( 'section1', 'Startpage' ) ) { 
		get_template_part( 'template_parts/content', 'innovativ' ); 
}
		endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

//teaser
$args = array(
	post_type => 'innovations',
);
$loop4= new WP_Query( $args );
if ( $loop4->have_posts() ) : while ( $loop4->have_posts() ) : $loop4->the_post();
	if( has_term( 'teaser', 'Startpage' ) ) { 	
get_template_part( 'template_parts/content', 'innoright' );
	}
	endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

//teaser1
$args = array(
	post_type => 'innovations',
);
$loop5 = new WP_Query( $args );
if ( $loop5->have_posts() ) : while ( $loop5->have_posts() ) : $loop5->the_post();
		if( has_term( 'teaser1', 'Startpage' ) ) { 
		get_template_part( 'template_parts/content', 'innoleft' ); 
		}
		endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
 endif; ?>

 <div class="slickslider">
<?php
//Gallery_top
$args = array(
	post_type => 'slider',
);
$loop6 = new WP_Query( $args );
if ( $loop6->have_posts() ) : while ( $loop6->have_posts() ) : $loop6->the_post();
		if( has_term( 'Gallery_top', 'Startpage' ) ) { 
		get_template_part( 'template_parts/content', 'slider' ); 
		}
		endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
 endif;	 
?>
	
  </div>
 </div>
<?php
//call_to_action2
$args = array(
	post_type => 'calltoaction',
);
$loop7 = new WP_Query( $args );
if ( $loop7->have_posts() ) : while ( $loop7->have_posts() ) : $loop7->the_post();
		if( has_term( 'call_to_action2', 'Startpage' ) ) { 
		get_template_part( 'template_parts/content', 'shinyblue' );
		}
	 endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;?>
 
<div id="Cost-effective"></div>
<?php
//section2
$args = array(
	post_type => 'innovations',
);
$loop8= new WP_Query( $args );
if ( $loop8->have_posts() ) : while ( $loop8->have_posts() ) : $loop8->the_post();
 if( has_term( 'section2', 'Startpage' ) ) { 	
 get_template_part( 'template_parts/content', 'innovativ' );
 }
 endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

//teaser2
$args = array(
	post_type => 'innovations',
);
$loop9 = new WP_Query( $args );
if ( $loop9->have_posts() ) : while ( $loop9->have_posts() ) : $loop9->the_post();
 if( has_term( 'teaser2', 'Startpage' ) ) { 		
get_template_part( 'template_parts/content', 'innoright_2' ); 
 }
endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

//call_to_action3
$args = array(
	post_type => 'calltoaction',
);
$loop10 = new WP_Query( $args );
if ( $loop10->have_posts() ) : while ( $loop10->have_posts() ) : $loop10->the_post();
 if( has_term( 'call_to_action3', 'Startpage' ) ){		
get_template_part( 'template_parts/content', 'shinyblue' );
 }
endwhile;
else : get_template_part( 'template_parts/content', 'error' );

endif;?>
<div id="Versatile"></div>

<?php
//section3
$args = array(
	post_type => 'innovations',
);
$loop11 = new WP_Query( $args );
if ( $loop11->have_posts() ) : while ( $loop11->have_posts() ) : $loop11->the_post();
 if( has_term( 'section3', 'Startpage' ) ){	
get_template_part( 'template_parts/content', 'innovativ' ); 
 }
 endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

$args = array(
	post_type => 'innovations',
);
$loop12 = new WP_Query( $args );
if ( $loop12->have_posts() ) : while ( $loop12->have_posts() ) : $loop12->the_post();
if( has_term( 'teaser3', 'Startpage' ) ){			
get_template_part( 'template_parts/content', 'innoright_3' );  
}
endwhile;
else : get_template_part( 'template_parts/content', 'error' );
endif;?>

 <div class="slickslider">
<?php
//gallery  bottom
$args = array(
	post_type => 'slider',
);
$loop13 = new WP_Query( $args );
if ( $loop13->have_posts() ) : while ( $loop13->have_posts() ) : $loop13->the_post();
		if( has_term( 'Gallery_bottom', 'Startpage' ) ) { 
		get_template_part( 'template_parts/content', 'sliderbottom' ); 
		}
		endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
 endif;
?>
  </div>
</div>

<?php

//call_to_action4 
$args = array(
	post_type => 'calltoaction',
);
$loop15 = new WP_Query( $args );
if ( $loop15->have_posts() ) : while ( $loop15->have_posts() ) : $loop15->the_post();
if( has_term( 'call_to_action4', 'Startpage' ) ) { 
get_template_part( 'template_parts/content', 'shinyblue' ); 
}
endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

//Newsletter
$args = array(
	post_type => 'innovations',	
);
$loop14 = new WP_Query( $args );
if ( $loop14->have_posts() ) : while ( $loop14->have_posts() ) : $loop14->the_post();
if( has_term( 'Newsletter', 'Startpage' ) ) { 
get_template_part( 'template_parts/content', 'newsletter' ); 
}
endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;

//call_to_action Competition 
$args = array(
	post_type => 'calltoaction',
);
$loop16 = new WP_Query( $args );
if ( $loop16->have_posts() ) : while ( $loop16->have_posts() ) : $loop16->the_post();
if( has_term( 'call_to_action_innovation', 'Startpage' ) ) { 
get_template_part( 'template_parts/content', 'competition' ); 
}
endwhile;
else : get_template_part( 'template_parts/content', 'error' ); 
endif;?>

<?php wp_reset_postdata(); ?>
<?php get_footer(); 