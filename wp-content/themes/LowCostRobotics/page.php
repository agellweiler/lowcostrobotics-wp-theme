<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 08.11.2017
 */
?>
<?php get_header(); ?>
<body>
	<nav id="menu">
		<div class="backlink">
			<div class="container"><a href="<?php echo get_site_url(); ?>">Back to the start page</a></div>
		</div>
	</nav>	
    <div id="top" class="shinyblue substage">
		<div class="container">
			<h1 class="heading style-h2"><?php the_title(); ?></h1>
		</div>
    </div>
	<a id="back-to-top" href="#top"></a>
    <div id="flexibel" class="container anchor">
		<section>
			<article>
				<div class="text">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					get_template_part( 'template_parts/content', 'page' ); endwhile;
					else :
					get_template_part( 'template_parts/content', 'error' ); 
					endif; ?>
				</div>
			</article>
		</section>	
	</div>
<?php get_footer(); ?>