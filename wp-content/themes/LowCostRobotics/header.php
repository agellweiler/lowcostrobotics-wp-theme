<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 08.11.2017
 */
?>
<!DOCTYPE html>
<html lang="<?php bloginfo('language');?>">
  <head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="description" content="<?php bloginfo('description');?>">
    <meta name="author" content="igus GmbH">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(''); ?> <?php bloginfo('name');?></title>
	<link href="<?php echo get_bloginfo('template_url'); ?>/css/slick.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/css/slick-theme.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/css/main.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<?php	wp_enqueue_script('jquery');?>
	<?php wp_head();?>
  </head>
  <body <?php body_class();?>>

