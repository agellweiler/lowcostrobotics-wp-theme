<?php /* Template Name: Order E-Book */ ?>
<?php get_header(); ?>
<body>
	<nav id="menu">
		<div class="backlink">
			<div class="container"><a href="<?php echo get_site_url(); ?>">Back to the start page</a></div>
		</div>
	</nav>	
	<?php
	$args = array(
		post_type => 'calltoaction',
		posts_per_page => 1,
		'offset' => 4
	);
	$loop1 = new WP_Query( $args );
	if ( $loop1->have_posts() ) : while ( $loop1->have_posts() ) : $loop1->the_post();
			?>
			<?php get_template_part( 'template_parts/content', 'shinyblue' ); ?>
		<?php endwhile;
	else : get_template_part( 'template_parts/content', 'error' );
		?>
<?php endif; ?>
	<div class="container">
		<section>
			<article>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template_parts/content', 'ebook' ); ?>
					<?php
					endwhile;
				else :
					?>
	<?php get_template_part( 'template_parts/content', 'error' ); ?>
<?php endif; ?>

			</article>
		</section>
	</div>
	<div id="newsletter">
		<div class="container">
			<section>
				<article class="grey-box">

					<div class="lcr_form">
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
							 <![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
							hbspt.forms.create({
								css: '',
								portalId: '2366840',
								formId: '47ba669c-b5dd-4847-8158-c9cd1f5f7215',
								target: ‘lcr_form’
							});
						</script>
					</div>


					<!--[if lte IE 8]>
			   <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
			   <![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
							 hbspt.forms.create({
								 css: '',
								 portalId: '2366840',
								 formId: '47ba669c-b5dd-4847-8158-c9cd1f5f7215'
							 });
					</script>
				</article>
			</section>
		</div>
	</div>

<?php get_footer(); ?>