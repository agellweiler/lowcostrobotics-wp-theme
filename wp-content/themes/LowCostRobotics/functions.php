<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
// Register Custom Post Type Call to Action
function post_type_calltoaction() {

	$labels = array(
		'name'                  => 'Call to Action Frontpage',
		'singular_name'         => 'Call to Action Frontpage',
		'menu_name'             => 'Call to Action Teaser',
		'name_admin_bar'        => 'Call to Action Teaser',
		'archives'              => 'Item Call to Action',
		'attributes'            => 'Item Call to Action',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Items',
		'add_new_item'          => 'Add New Item',
		'add_new'               => 'Add New',
		'new_item'              => 'New Item',
		'edit_item'             => 'Edit Item',
		'update_item'           => 'Update Item',
		'view_item'             => 'View Item',
		'view_items'            => 'View Items',
		'search_items'          => 'Search Item',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'Call to Action Frontpage',
		'description'           => 'Textinformation for Call to Action Teaser',
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
        'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'calltoaction', $args );

}
add_action( 'init', 'post_type_calltoaction', 0 );

// Register Custom Post Type Teaser
function post_type_innovations() {

	$labels = array(
		'name'                  => 'Teaser for the Frontpage',
		'singular_name'         => 'Teaser for the Frontpage',
		'menu_name'             => 'Teaser for the Frontpage ',
		'name_admin_bar'        => 'Teaser for the Frontpage',
		'archives'              => 'Teaser Item',
		'attributes'            => 'Teaser Item',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Items',
		'add_new_item'          => 'Add New Item',
		'add_new'               => 'Add New',
		'new_item'              => 'New Item',
		'edit_item'             => 'Edit Item',
		'update_item'           => 'Update Item',
		'view_item'             => 'View Item',
		'view_items'            => 'View Items',
		'search_items'          => 'Search Item',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => ' Teaser on Frontpage',
		'description'           => 'Textinformation for Teaser on the frontpage',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
        'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'innovations', $args );

}
add_action( 'init', 'post_type_innovations', 0 );

//Sleder Post type
function post_type_slider() {

	$labels = array(
		'name'                  => 'Slider for the Frontpage',
		'singular_name'         => 'Slider for the Frontpage',
		'menu_name'             => 'Slider for the Frontpage ',
		'name_admin_bar'        => 'Slider for the Frontpage',
		'archives'              => 'Slider Item',
		'attributes'            => 'Slider Item',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Items',
		'add_new_item'          => 'Add New Item',
		'add_new'               => 'Add New',
		'new_item'              => 'New Item',
		'edit_item'             => 'Edit Item',
		'update_item'           => 'Update Item',
		'view_item'             => 'View Item',
		'view_items'            => 'View Items',
		'search_items'          => 'Search Item',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'Slider Elements for Frontpage',
		'description'           => 'Slider Elements for the frontpage',
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
        'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'slider', $args );

}
add_action( 'init', 'post_type_slider', 0 );

// Register Custom Taxonomy
function lcr_ct_start() {

	$labels = array(
		'menu_name'                  => 'Startpage',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'Startpage', array( 'calltoaction', 'innovations' , 'slider'), $args );

}
add_action( 'init', 'lcr_ct_start', 0 );

// Register Custom Taxonomy
function lcr_ct_slider() {

	$labels = array(
		'menu_name'                  => 'Slider-Asset',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'slider', array( 'slider'), $args );

}
add_action( 'init', 'lcr_ct_slider', 0 );

//Navigation

add_action('after_setup_theme', 'lcr_register_nav');
	function lcr_register_nav() {
		register_nav_menu('nav_main', 'Horizontal Navigation Top');
		register_nav_menu('nav_footer', 'Footer Navigation');
	}
	

if ( ! function_exists( 'theme_slug_setup' ) ) :
    /**
     * Sets up theme and registers support for various WordPress features.
     */
    function theme_slug_setup() {
        // Other Theme Setup code...

        // Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );

        // Set detfault Post Thumbnail size.
        set_post_thumbnail_size( 300, 200, true );

        // Add custom image size for single posts.
        add_image_size( 'theme-slug-single-post', 800, 9999 );
    }
endif;
add_action( 'after_setup_theme', 'theme_slug_setup' );

//Styles für den Admin-Bereich
function kb_admin_style() {
   wp_enqueue_style('admin-styles', get_template_directory_uri().'/style-admin.css');
}

add_action('admin_enqueue_scripts', 'kb_admin_style');


