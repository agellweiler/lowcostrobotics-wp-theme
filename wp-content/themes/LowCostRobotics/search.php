<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 20.11.2017
 */
?>
<?php get_header(); ?>
<body>
	<nav id="menu">
		<div class="backlink">
			<div class="container"><a href="<?php echo get_site_url(); ?>">Back to the start page</a></div>
		</div>
	</nav>	
    <div id="top" class="shinyblue substage">
		<div class="container">
			<h1 class="heading style-h2">Search results for: <?php echo $s;?></h1>
		</div>
    </div><a id="back-to-top" href="#top"></a>
    <div id="flexibel" class="container anchor">
		<section>
			<article>
				<div class="text">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
         <h5 class="heading style-h2"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
            <p>created on: <?php the_date('d.m.Y'); ?> | 
            by: <?php the_author(); ?> | 
            Categories: <?php echo get_the_term_list( $post->ID, 'Startpage' ); ?></p>
            <?php the_content(); ?>
      <?php endwhile; endif; ?>
       
   </div>
			</article>
		</section>	
	</div>
 

<?php get_footer(); ?>

