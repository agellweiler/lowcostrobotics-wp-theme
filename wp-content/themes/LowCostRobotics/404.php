<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 20.11.2017
 */
get_header(); ?>
<body>
	<nav id="menu">
		<div class="backlink">
			<div class="container"><a href="<?php echo get_site_url(); ?>">Back to the start page</a></div>
		</div>
	</nav>	
<div id="top" class="shinyblue substage">
		<div class="container">
			<h1 class="heading style-h2">Error 404</h1>
		</div>
    </div><a id="back-to-top" href="#top"></a>
    <div id="flexibel" class="container anchor">
		<section>
			<article>
				<div class="text">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'lcr' ); ?></h1>
				</header><!-- .page-header -->
				<div class="page-content">
					
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'lcr' ); ?></p>
					<?php get_search_form(); ?>
					<center><img alt="Robot_icon_broken.png" src="https://embed.widencdn.net/img/igus/lc8h8krp5h/400x400px/Robot_icon_broken.png?crop=false&position=c&color=ffffff00&u=9c3pzs"></center>
				</div><!-- .page-content -->
				</div>
			</article>
		</section><!-- .error-404 -->
	</div><!-- #primary -->

<?php get_footer();

