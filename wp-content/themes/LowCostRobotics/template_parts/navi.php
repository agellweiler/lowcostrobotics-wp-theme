   <!--Movile navigation-->
    <nav id="mobile-menu" class="navbar navbar-fixed-top visible-xs">
      <!-- Brand and toggle get grouped for better mobile display-->
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#mobile-menu-collapse" aria-expanded="false" class="navbar-toggle collapsed"><span class="sr-only">Navigation öffnen</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        <div class="navbar-brand"><strong>Low Cost Robotics</strong><br><span>Strong Stable Government</span></div>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling-->
      <div id="mobile-menu-collapse" class="navbar-collapse collapse">
        <ul>
          <li><a href="#Innovative">INNOVATIVE</a></li>
          <li><a href="#Cost-effective">COST-EFFECTIVE</a></li>
          <li><a href="#Versatile">VERSATILE</a></li>
          <li><a href="<?php echo get_site_url(); ?>/contact-us/">Contact</a></li>
        </ul>
      </div>
      <!-- /.navbar-collapse-->
    </nav>
    <!--Header-->
    <div id="top" class="shinyblue anchor">
      <article id="stage">
        <h1 class="hidden-xs"><span class="lc"><?php bloginfo('name');?></span></h1>
        <blockquote class="col-sm-push-2 col-sm-6">
          <p><?php bloginfo('description');?></p>
        </blockquote>
      </article>
    </div>
    <!--Back to top
    a#back-to-top(href='#top', title='Zum Anfang der Seite')
    
    -->
    <!-- Nav menu-->
    <div class="menu-placeholder hidden-xs">
      <nav id="menu"><a href="<?php echo get_site_url(); ?>/contact-us/" class="besides pull-right">Contact</a>
        
          <?php
		   $args = array(
			   'theme_location' => 'nav_main',
			   'depth' => 1
		   );
		   wp_nav_menu($args);
		?>
			
      </nav>
      <div class="arrow-down hidden-xs"></div>
    </div>
