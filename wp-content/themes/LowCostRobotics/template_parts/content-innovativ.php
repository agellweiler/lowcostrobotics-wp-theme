<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>


     <div class="container anchor">
		         <section>
		<header>
         <div class="text">
			<section <?php post_class(); ?>>
				<h2 class="heading style-h2"><?php the_title();?></h2>
				<?php the_content(); ?> 
			</section>	
			 </div>
		</header>
				 	
	
