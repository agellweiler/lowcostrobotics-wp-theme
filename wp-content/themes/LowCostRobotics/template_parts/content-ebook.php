<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>
<section>
	
<h1 class="heading style-h3"><?php the_title();?></h1>
			<?php the_content();?> 
</section>
