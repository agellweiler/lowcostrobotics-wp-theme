<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>

<article class="flex box half crop">
	<div class="img first">
		<img alt="MenschRoboter-767w.jpg" src="https://embed.widencdn.net/img/igus/4r6w8kvlr3/556x370px/MenschRoboter-767w.jpg?crop=false&position=c&color=ffffffff&u=9c3pzs" class="img-responsive"/></div>
	<div class="text second">
		<section <?php post_class(); ?>>
			<h3 class="heading style-h3"><?php the_title(); ?></h3>
			<?php the_content(); ?> 
		</section>	
	</div>
</article>
</div>
