<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>
<div class="container">
<section>
<article class="flex box half crop">
	<div class="img second">
		<img alt="robolink_contest.png" src="https://embed.widencdn.net/img/igus/sjdwcacwxl/400x400px/robolink_contest.png?crop=false&position=c&color=ffffff00&u=9c3pzs">
	</div>
	<div class="text first">
		<section <?php post_class(); ?>>
			<h3 class="heading style-h3"><?php the_title(); ?></h3>
			<?php the_content(); ?> 
			<p>
			<a href="<?php echo get_permalink( 29 ); ?>" title="contact-us" class="cta-btn orange">Contact us!</a>
			</p>
		</section>	
	</div>
</article>
</section>
</div>	

				 	
	
