<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>
<!--Intro to Indexpage-->
<article id="intro" class="flex box half container">
	<div class="first intro">
        <div class="text">
			<section <?php post_class(); ?>>
				<h1 class="heading style-h2"><?php the_title(); ?></h1>
				<?php the_content(); ?> 
			</section>
		</div>
	</div>
	<div class="img second"><img src="https://embed.widencdn.net/img/igus/emdvzyqzix/668x512px/Intro_AutomationZukunft.jpeg?position=c&amp;crop=no&amp;color=ffffffff&amp;u=9c3pzs" alt="Intro_AutomationZukunft" width="668" height="512" /></div>
</article>			

