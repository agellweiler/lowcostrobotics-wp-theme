<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 17.11.2017
 */


if (has_term('slider1', 'slider')) {
	$emb = "https://embed.widencdn.net/img/igus/mfsmqjuerd/410x312px/Anwendungsbeispiel_Alexa-Fraunhofer-717w.jpg?crop=false&position=c&color=ffffff00&u=9c3pzs";
}
if (has_term('slider2', 'slider')) {
	$emb = 'https://p.widencdn.net/ix8pet/robolink-underwater';
}
if (has_term('slider3', 'slider')) {
	$emb = 'https://p.widencdn.net/ugqexz/robolink-application---pick-and-place';
}

if (has_term('slider4', 'slider')) {
	$emb = 'https://p.widencdn.net/8wn5so/Bookscanner-with-Automated-Pageturn-Robot-Low-Cost-Robotic-Application';
}
?>

<div>
<?php
if (has_tag('slider1')) {
?>
		<article class="flex half">
			<div class="img second">
				<img src="<?php echo $emb; ?>">
			</div>

<?php
} else {
	?>


			<article class="flex half">
				<div class="img second">
					<div class="youtube-embed">
						<iframe src="<?php echo $emb; ?>"
								class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
<?php }
?>

			
			<div class="text first">	
				<h4 class="heading style-h3"><?php the_title(); ?></h4>
				<section>
<?php the_content(); ?> 
				</section>	
			</div>
		</article> 
</div>



					 
