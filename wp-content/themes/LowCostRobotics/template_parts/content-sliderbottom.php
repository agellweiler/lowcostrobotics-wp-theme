<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 17.11.2017
 */

if (has_term('slider5', 'slider')) {
	$emb="https://embed.widencdn.net/img/igus/m5k9m7k04h/510x340px/Effizienz-steigern_EPS-510w.jpg?crop=false&position=c&color=ffffffff&u=9c3pzs";
}
 if (has_term('slider6', 'slider')){
	$emb='https://embed.widencdn.net/img/igus/g7fjnuh5pu/410x273px/Effizienz-steigern_Roland-Behrens-Fraunhofer-410w.jpg';	
}
?>
<div>
	<article class="flex half">
		<div class="img second">
                                    <img src="<?php echo $emb;?>">  
                                </div>
		<div class="text first">	
			<h4 class="heading style-h3"><?php the_title(); ?></h4>
			<section>
				<?php the_content(); ?> 
			</section>	
		</div>
	</article> 
</div>		

					 