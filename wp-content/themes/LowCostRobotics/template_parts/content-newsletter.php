<?php
/*
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>

<div id="newsletter">
    <div class="container">
        <article class="grey box">
            <div class="flex cta">
                <div class="text first big">
					<section <?php post_class(); ?>>
						<h5 class="heading style-h3"><?php the_title(); ?></h5>
						<?php the_content(); ?> 
					</section>
                </div>
                <div class="button second small">
                    <button class="cta-btn white signon" title="Zum Newsletter anmelden" onclick="showDiv()">
						Subscribe to newsletter
                    </button>

                </div>
			</div>			
			<div id="newsletterform" style="display:none;"> 		

				<!--[if lte IE 8]>

<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>

<![endif]-->

				<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
				<script>
                        hbspt.forms.create({
                            css: '',
                            portalId: '2366840',
                            formId: '4c0a6845-fd3d-445d-a0ce-f47b89445771'
                        });

				</script>
            </div>
		</article>
	</div>	
</div>



