<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>
<!--Call to Action-->
<div class="shinyblue ebook ebook-full">
	<div class="container">
        <div class="flex cta"><img alt="badge-book_trans.png" src="https://embed.widencdn.net/img/igus/zgechq0icl/268x268px/badge-book_trans.png?crop=false&position=c&color=ffffff00&u=9c3pzs" class="first small"/>
			<div class="text second big">
				<header>
					<section <?php post_class(); ?>>
						<h5 class="heading style-h2"><?php the_title();?></h5>
						<?php the_content(); ?> 
					</section>
				</header><a href="<?php echo get_permalink(57); ?>" title="Request e-book free of charge" class="cta-btn orange">Free Cake!</a>
			</div>
        </div>
	</div>
</div>
