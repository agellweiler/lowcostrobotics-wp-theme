<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>
<article class="flex box half crop">
	<div class="img second">
		<img alt="WaskosteteinRoboterarm-570w.jpg" src="https://embed.widencdn.net/img/igus/hfafpnzzri/570x380px/WaskosteteinRoboterarm-570w.jpg?crop=false&position=c&color=ffffffff&u=9c3pzs" class="img-responsive"/>
	</div>
	<div class="text first">
		<section <?php post_class(); ?>>
			<h3 class="heading style-h3"><?php the_title(); ?></h3>
			<?php the_content(); ?> 
			<p>
			<a href="<?php echo get_site_url(); ?>/how-much-does-a-robotic-arm-cost" class="readmore">Learn more</a>
			</p>
		</section>	
	</div>
</article>
</div>

