<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 09.11.2017
 */
?>
<article class="flex box half crop">
	<div class="img second">
		<img alt="RobolinkPrint_freigestellt-570w.png" src="https://embed.widencdn.net/img/igus/8wighqcmjs/570x566px/RobolinkPrint_freigestellt-570w.png?crop=false&position=c&color=ffffff00&u=9c3pzs" class="img-responsive"/>
	</div>
	<div class="text first">
		<section <?php post_class(); ?>>
			<h3 class="heading style-h3"><?php the_title(); ?></h3>
			<?php the_content(); ?> 
			<p>
			<a href="<?php echo get_site_url(); ?>/comparing-modular-design-and-conventional-systems" class="readmore">Learn more</a>
			</p>
		</section>	
	</div>
</article>

