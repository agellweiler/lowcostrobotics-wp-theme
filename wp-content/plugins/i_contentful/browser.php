<?php
/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 23.10.2017
 */ 
echo "<div class=\"wrap\"><h1>Artikel aus Contentful für den Blog auswählen</h1></div>";
$api_key1 = get_site_option('contentful_api');
$accessToken = $api_key1['api_key'];
$spaceID = $api_key1['space_id'];
$headline_contenttype = $api_key1['contenttype'];
$headline_field = 'get'.$api_key1['headline_field'];
// $creation_field = 'get'.$api_key1['creation_field'];
$teaser_field = 'get'.$api_key1['maintext_field'];

echo $headline_contenttype;
echo $teaser_field;

use Contentful\Delivery\Client;
require_once __DIR__ . '/vendor/autoload.php';
$client = new Client($accessToken, $spaceID);  
$query = new \Contentful\Delivery\Query;
$query->setContentType($headline_contenttype);
    //->orderBy('fields.created', true);
$EntriesTitle = $client->getEntries($query);
foreach ($EntriesTitle as $titlelist)
    {
                //echo $headline_field;
                $imageWidth ='';
		$imageHeight ='';
		$imageUrl ='';
		$imageTitle ='';
		$entryId = $titlelist->getId();
        $title = $titlelist->$headline_field();
               // $createdID = $titlelist->$creation_field();
        $teaser =$titlelist->$teaser_field();
		//$frontImage = $titlelist->getteaserbild();
//		if (!empty($frontImage)) {
//		$imageTitle = $frontImage->getTitle();	
//		$imageUrl = $frontImage->getFile()->getUrl();
//		$imageWidth = $frontImage->getFile()->getWidth();
//		$imageHeight = $frontImage->getFile()->getHeight();
//		}
 ?>
<a href="admin.php?page=contentful-details&AMP;id=<?php echo $entryId;?>"><h3><?php echo $title;?></h3></a>
<p>
<?php if (!empty($frontImage)) {?>	
<img src="<?php echo 'http:'.$imageUrl.'';?>" alt="<?php echo $imageTitle;?>" style="width: <?php echo $imageWidth.'px';?>;height: <?php echo $imageHeight.'px';?>" class="img-thumbnail img-responsive pull-left abstand-rechts">
<?php }
?>
<strong>Erstellt am:</strong> <?php //echo $createdID->format(DateTime::RFC1123).  PHP_EOL; ?><br>
<?php echo $teaser;?></p>
<a href="admin.php?page=contentful-details&AMP;id=<?php echo $entryId;?>">
<button class="button button-primary">
Ganzen Artikel prüfen
</button> </a>   
<?php
       }