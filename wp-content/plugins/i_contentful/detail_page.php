<?php

/* 
 * Armin Gellweiler
 * agellweiler@igus.net
 * 24.10.2017
 */
require_once __DIR__ . '/Parsedown.php';
if (isset($_GET['id'])) {
    $entryId = $_GET['id'];
}

if (empty($entryId)){
?>
<p>Um komplette Artikel aus Contentful anzuzeigen und auszuwählen, muss zuerst in der Artikelliste ein passender Eintrag ausgewählt werden.</p>
<a href="admin.php?page=contentful-browser">
    <button class="button button-primary">
     Browse Contentful
    </button>
</a>
<?php
}
else {  
?>  
<p>
  <form action="#c_form" method="post" id="c_form">
        <input type="hidden" name="visitor_name" id="visitor_name" value="test"/>
        <input type="submit" name="submit_form" value="submit" />
    </form>
</p>
<?php    
$api_key1 = get_site_option('contentful_api');
$accessToken = $api_key1['api_key'];
$spaceID = $api_key1['space_id']; 
require_once __DIR__ . '/vendor/autoload.php';
$kurztext_field = 'get'.$api_key1['kurztext_field'];
$maintext_field = 'get'.$api_key1['maintext_field'];
$client = new \Contentful\Delivery\Client($accessToken, $spaceID);
$entry = $client->getEntry($entryId);
$title = $entry->getheadlineMain();
$description = $entry->gettopic();
$text = $entry->$kurztext_field();
$maintext= $entry->$maintext_field();
$product = $entry->getproduct();
$industrie = $entry->getindustrie();
$creation = $entry->getcreated();
$author = $entry->getauthor();
$contact = $entry->getcontactIgus();
$usp = $entry->getUSPsMain();
$proof = $entry->getproof();
$subhead = $entry->getsubhead();
$headlineShort = $entry->getheadlineShort();
$socialText = $entry->getsocialText();
$bilder = $entry->getbilderammlung();
$teasertext = $entry->getteaser();

$main = new Parsedown();

                    
 // does the inserting, in case the form is filled and submitted

    if ( isset( $_POST["submit_form"] ) && $_POST["visitor_name"] != "" ) {
       
        $table = $wpdb->prefix."wp_posts";
        $GLOBALS['wpdb']->insert(
            $table,
 ['post_title' => $title,
        'post_content' => $main->text($maintext),  
        'post_date' =>$creation->format("Y-m-d H-i-s"),   
        'post_name' => sanitize_title_with_dashes($title,'','save'),     
        'post_status' => 'publish',
        'post_type' => 'i_contentful',    
        'post_author' => 1,
        //'post_category' => array(7,8)
        ]
        );
        
        echo '<p><font color="GREEN">Vielen Dank - Der Artikel wurde unter dem Posttype Contentful Artikel abgespeichert. Er wird im Menü links unter "Contentful Artikel" gelistet.</font></p>';
    }

?>

    <strong>Topic:</strong> <?php echo "$description"; ?><br>
    <strong>Erstellt am:</strong> <?php echo $creation->format(DateTime::RFC1123).  PHP_EOL; ?><br>e
    <strong>Autor:</strong> <?php echo "$author"; ?><br>
    <strong>igus-Kontakt:</strong> <?php echo "$contact"; ?><br>
    <strong>Kurze Headline:</strong> <?php echo "$headlineShort"; ?><br>
    <strong>Produkte im Artikel:</strong> <?php echo "$product"; ?>
    <h2><small><?php echo "$subhead"; ?></small></h2> 
    <h2><?php echo "$title"; ?></h2>
    <h4>Kurztext:</h4>
    <p>
    <?php
         if (empty($text))
         {
             echo 'Kurztext noch nicht erfasst';
         }
       $Parsedown = new Parsedown();
       echo $Parsedown->text($text);
 ?>
     </p>
     <h4>Langtext:</h4>
     <p>
         <?php
          if (empty($maintext))
         {
             echo 'Lantext noch nicht erfasst';
         }					
            $main = new Parsedown();
            echo $main->text($maintext);
            ?>
     </p>
<?php
   
}

