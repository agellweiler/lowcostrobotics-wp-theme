<?php
defined('ABSPATH') or die("No script kiddies please!");
/**
* Plugin Name: Igus Contentul Adpater to Wordpress
* Plugin URI: http://
* Description:  A plugin that connects to Contentful
* Version: 0.0.1
* Author: Armin Gellweiler
* Author URI: http://armin.gellweiler.net
* License: GPL2
*/

class ContentfulSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Contentful-API', 
            'manage_options', 
            'contentful-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'contentful_api' );
        ?>
        <div class="wrap">
            <h1>Contentful Connector  - Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'contentful_option_group' );
                do_settings_sections( 'contentful-setting-admin' );
                submit_button();
            ?>
            </form>	
        </div>
<?php 


    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'contentful_option_group', // Option group
            'contentful_api', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Step1: Settings for the Contentful AP', // Title
            array( $this, 'print_section_info' ), // Callback
            'contentful-setting-admin' // Page
        );  

        add_settings_field(
            'api_key', // API-Key
            'API-KEY', // Beschreibung
            array( $this, 'api_key_callback' ), // Callback
            'contentful-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'space_id', 
            'Contentspace', 
            array( $this, 'space_id_callback' ), 
            'contentful-setting-admin', 
            'setting_section_id'
        ); 
		
		 add_settings_field(
            'contenttype', 
            'Contenttype', 
            array( $this, 'contenttype_callback' ), 
            'contentful-setting-admin', 
            'setting_section_id'
        ); 
          
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['api_key'] ) )
            $new_input['api_key'] = sanitize_text_field( $input['api_key'] );

        if( isset( $input['space_id'] ) )
            $new_input['space_id'] = sanitize_text_field( $input['space_id'] );
		
		if( isset( $input['contenttype'] ) )
            $new_input['contenttype'] = sanitize_text_field( $input['contenttype'] );
        

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Please enter the Keys and  field names from Contentful in the fields.'
        . '<br>
        <strong><font color="#FF0000">Attention:</strong></font> Please make sure that the information is correct. ';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function api_key_callback()
    {
        printf(
            '<input type="text" id="api_key" size="100" name="contentful_api[api_key]" value="%s" />',
            isset( $this->options['api_key'] ) ? esc_attr( $this->options['api_key']) : ''
        );
    }
    /** 
     * Get the settings option array and print one of its values
     */
    public function space_id_callback()
    {
        printf(
            '<input type="text" id="space_id" name="contentful_api[space_id]" value="%s" />',
            isset( $this->options['space_id'] ) ? esc_attr( $this->options['space_id']) : ''
        );
    }
	
	public function contenttype_callback()
    {
        printf(
            '<input type="text" id="contenttype" name="contentful_api[contenttype]" value="%s" />',
            isset( $this->options['contenttype'] ) ? esc_attr( $this->options['contenttype']) : ''
        );
    }
	
}

if( is_admin() )
    $contentful_api_key = new ContentfulSettingsPage();


function contentful_options_panel(){
  add_menu_page('Contentful-page title', 'Browse Contentful', 'manage_options', 'contentful-browser', 'contentful_browse_func');
  add_submenu_page( 'contentful-browser', 'Contenttype', 'Select Contenttype', 'manage_options', 'contentful-contenttype', 'contentful_browse_func_contenttype');
  add_submenu_page( 'contentful-browser', 'Detailseite', 'Detailseite', 'manage_options', 'contentful-details', 'contentful_browse_func_settings');
  add_submenu_page( 'contentful-browser', 'FAQ', 'FAQ', 'manage_options', 'contentful-faq', 'contentful_browse_func_faq');
}
add_action('admin_menu', 'contentful_options_panel');
 
function contentful_browse_func(){
                echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
                <h2>Contentful-Browser</h2></div>';
                include ("browser.php");
}   
function contentful_browse_func_contenttype(){
                echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
                <h2>Select Contenttype</h2></div>';
                include ("contenttype.php");
}  
function contentful_browse_func_settings(){
                echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
                <h2>Detailseite</h2></div>';
                include ("detail_page.php");
}
function contentful_browse_func_faq(){
                echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
                <h2>FAQ</h2></div>';
}

add_action( 'pre_get_posts', 'add_my_post_types_to_query' );
 
function add_my_post_types_to_query( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'post', 'i_contentful' ) );
    return $query;
}


